﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public GameObject currentCheckPoint;

	private PlayerController player;

	public GameObject deathParticle;
	public GameObject respawnParticle;

	public int pointPenaltyOnDeath;

	public float respawnDelay;

	private float gravityStore;
	
	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController> ();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void RespawnPlayer()
	{
		StartCoroutine ("respawnPlayerCo");
	}

	public IEnumerator respawnPlayerCo()
	{
		Instantiate (deathParticle, player.transform.position, player.transform.rotation); //create copy 
		player.enabled = false;
		player.GetComponent<Renderer> ().enabled = false;

		gravityStore = player.GetComponent<Rigidbody2D> ().gravityScale;
		player.GetComponent<Rigidbody2D> ().gravityScale = 0f;
		player.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		ScoreManager.AddPoint (-pointPenaltyOnDeath);

		Debug.Log ("Player Respawn");
		yield return new WaitForSeconds (respawnDelay);
		player.GetComponent<Rigidbody2D> ().gravityScale = gravityStore;
		player.transform.position = currentCheckPoint.transform.position;
		player.enabled = true;
		player.GetComponent<Renderer> ().enabled = true;
		Instantiate (respawnParticle, currentCheckPoint.transform.position, currentCheckPoint.transform.rotation);

	}
}
